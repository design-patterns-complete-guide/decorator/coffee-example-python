# Step 1: Define the Coffee interface (Component)
from abc import ABC, abstractmethod

class Coffee(ABC):
    @abstractmethod
    def cost(self):
        pass

# Step 2: Create a Concrete Component (BasicCoffee)
class BasicCoffee(Coffee):
    def cost(self):
        return 5.0  # Basic coffee costs $5.0

# Step 3: Create an abstract Decorator class
class CoffeeDecorator(Coffee):
    def __init__(self, coffee):
        self._coffee = coffee

    def cost(self):
        return self._coffee.cost()

# Step 4: Create Concrete Decorator classes
class MilkDecorator(CoffeeDecorator):
    def cost(self):
        return super().cost() + 2.0  # Add the cost of milk ($2.0)

class SugarDecorator(CoffeeDecorator):
    def cost(self):
        return super().cost() + 1.0  # Add the cost of sugar ($1.0)

# Step 5: Client code
coffee = BasicCoffee()
coffee_with_milk = MilkDecorator(coffee)
coffee_with_milk_and_sugar = SugarDecorator(coffee_with_milk)

print("Cost of Basic Coffee: $" + str(coffee.cost()))
print("Cost of Coffee with Milk: $" + str(coffee_with_milk.cost()))
print("Cost of Coffee with Milk and Sugar: $" + str(coffee_with_milk_and_sugar.cost()))
